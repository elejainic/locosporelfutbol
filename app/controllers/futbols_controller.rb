class FutbolsController < ApplicationController
  helper_method :suma_resultados
  
  def index
    @futbol = Futbol.paginate(:page => params[:page], :per_page => 7)
  end
  
  
  def new
    @futbol = Futbol.new
  end

  def create
    
    @futbol = Futbol.new(futbol_params)
 
    if @futbol.save
      redirect_to root_path
      else
      render 'new'
    end
    end

  def show
    @futbol = Futbol.find(params[:id])
  
      respond_to do |format|
      format.html 
      format.js
      format.json { render json: @futbol }
    end

  end
  
  def edit
    @futbol = Futbol.find(params[:id])
  end
  
  def update
    @futbol = Futbol.find(params[:id])
 
    if @futbol.update(futbol_params)
      redirect_to @futbol
    else
      render 'edit'
    end
  end
  
  def destroy
    @futbol = Futbol.find(params[:id])
    @futbol.destroy
 
    redirect_to root_path
  end
  
  def suma_resultados
    total_count = 0
    @futbol = Futbol.find(params[:id])
    total_count = @futbol.unoxdosuno + @futbol.unoxdosequix
    total_count
  end
  
  private
  def futbol_params
    params.require(:futbol).permit(:local, :visitante, :unoxdosuno, :unoxdosequix, :unoxdosdos, :column_fecha)
  end
end
