class BetsController < ApplicationController
       def index
  @game = Game.find(params[:game_id])
  @bets = @game.bets
end

def new
    @variable = 6
   @game = Game.find(params[:game_id])
  @bet = @game.bets.new
end

def show
    @bet = Bet.find(params[:id])
end

def create
    @variable = 6
    @game = Game.find(params[:game_id])
    @bet = @game.bets.new(bet_params)
    if( @bet.cotizacion == 10)
        @bet.valor = "1 - 0"
        @bet.cotizacion = @game.unoxcero
    elsif ( @bet.cotizacion == 20)
        @bet.valor = "2 - 0"
        @bet.cotizacion = @game.dosxcero
        
        elsif ( @bet.cotizacion == 21)
        @bet.valor = "2 - 1"
        @bet.cotizacion = @game.dosxuno
        
        elsif ( @bet.cotizacion == 30)
        @bet.valor = "3 - 0"
        @bet.cotizacion = @game.tresxcero
        
        elsif ( @bet.cotizacion == 31)
        @bet.valor = "3 - 1"
        @bet.cotizacion = @game.tresxuno
        elsif ( @bet.cotizacion == 32)
        @bet.valor = "3 - 2"
        @bet.cotizacion = @game.tresxdos
        
        elsif ( @bet.cotizacion == 40)
        @bet.valor = "4 - 0"
        @bet.cotizacion = @game.cuatroxcero
        
        elsif ( @bet.cotizacion == 41)
        @bet.valor = "4 - 1"
        @bet.cotizacion = @game.cuatroxuno
        
        elsif ( @bet.cotizacion == 42)
        @bet.valor = "4 - 2"
        @bet.cotizacion = @game.cuatroxdos
        elsif ( @bet.cotizacion == 43)
        @bet.valor = "4 - 3"
        @bet.cotizacion = @game.cuatroxtres
        elsif ( @bet.cotizacion == 50)
        @bet.valor = "5 - 0"
        @bet.cotizacion = @game.cincoxcero
        elsif ( @bet.cotizacion == 00)
        @bet.valor = "0 - 0"
        @bet.cotizacion = @game.ceroxcero
        elsif ( @bet.cotizacion == 11)
        @bet.valor = "1 - 1"
        @bet.cotizacion = @game.unoxuno
        elsif ( @bet.cotizacion == 22)
        @bet.valor = "2 - 2"
        @bet.cotizacion = @game.dosxdos
        elsif ( @bet.cotizacion == 33)
        @bet.valor = "3 - 3"
        @bet.cotizacion = @game.tresxtres
        elsif ( @bet.cotizacion == 44)
        @bet.valor = "4 - 4"
        @bet.cotizacion = @game.cuatroxcuatro
        elsif ( @bet.cotizacion == 01)
        @bet.valor = "0 - 1"
        @bet.cotizacion = @game.ceroxuno
        elsif ( @bet.cotizacion == 02)
        @bet.valor = "0 - 2"
        @bet.cotizacion = @game.ceroxdos
        elsif ( @bet.cotizacion == 12)
        @bet.valor = "1 - 2"
        @bet.cotizacion = @game.unoxdos
        elsif ( @bet.cotizacion == 03)
        @bet.valor = "0 - 3"
        @bet.cotizacion = @game.ceroxtres
        elsif ( @bet.cotizacion == 13)
        @bet.valor = "1 - 3"
        @bet.cotizacion = @game.unoxtres
        elsif ( @bet.cotizacion == 23)
        @bet.valor = "2 - 3"
        @bet.cotizacion = @game.dosxtres
        elsif ( @bet.cotizacion == 04)
        @bet.valor = "0 - 4"
        @bet.cotizacion = @game.ceroxcuatro
        elsif ( @bet.cotizacion == 14)
        @bet.valor = "1 - 4"
        @bet.cotizacion = @game.unoxcuatro
        elsif ( @bet.cotizacion == 24)
        @bet.valor = "2 - 4"
        @bet.cotizacion = @game.dosxcuatro
        elsif ( @bet.cotizacion == 34)
        @bet.valor = "3 - 4"
        @bet.cotizacion = @game.tresxcuatro
        elsif ( @bet.cotizacion == 50)
        @bet.valor = "5 - 0"
        @bet.cotizacion = @game.cincoxcero
        end
         
     @bet.resultado =(@bet.intro) * (@bet.cotizacion)
     @bet.random = SecureRandom.hex(5)
  if @bet.save 
    redirect_to  game_bet_url(@game,@bet),
                  notice: "Thanks for your review!"
  else
    render :new
  end
  
 

end

private
  def bet_params
    params.require(:bet).permit(:random, :cotizacion, :resultado, :intro, :valor)
  end

end
