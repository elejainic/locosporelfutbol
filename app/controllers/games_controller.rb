class GamesController < ApplicationController
    
  def index
      @game = Game.paginate(:page => params[:page], :per_page => 7)
  end
      
  def new
    @game = Game.new
  end

  def create
    
    @game = Game.new(game_params)
 
    if @game.save
      redirect_to games_path
      else
      render 'new'
    end
    end

  def show
    @game = Game.find(params[:id])
  
      respond_to do |format|
      format.html 
      format.js
      format.json { render json: @game }
    end

  end
  
  def edit
    @game = Game.find(params[:id])
  end
  
  def update
    @game = Game.find(params[:id])
 
    if @game.update(game_params)
      redirect_to @game
    else
      render 'edit'
    end
  end
  
  def destroy
    @game = Game.find(params[:id])
    @game.destroy
 
    redirect_to root_path
  end
  
#   def suma_resultados
#     total_count = 0
#     @futbol = Futbol.find(params[:id])
#     total_count = @futbol.unoxdosuno + @futbol.unoxdosequix
#     total_count
#   end
  
  private
  def game_params
    params.require(:game).permit(:local, :visitante, :unoxcero, :dosxcero, :dosxuno, :tresxcero, :tresxuno, :tresxdos, :cuatroxcero, :cuatroxuno, :cuatroxdos, :cuatroxtres, :cincoxcero, :ceroxcero, :unoxuno, :dosxdos, :tresxtres, :cuatroxcuatro, :ceroxuno, :ceroxdos, :unoxdos, :ceroxtres, :unoxtres, :dosxtres, :ceroxcuatro, :unoxcuatro, :dosxcuatro, :tresxcuatro, :otro, :cantidad, :fecha )
  end
end
