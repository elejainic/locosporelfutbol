class ReviewsController < ApplicationController
    

    def index
  @futbol = Futbol.find(params[:futbol_id])
  @reviews = @futbol.reviews
end

def new
    @variable = 6
   @futbol = Futbol.find(params[:futbol_id])
  @review = @futbol.reviews.new
end

def show
    @review = Review.find(params[:id])
end

def create
    @variable = 6
    @futbol = Futbol.find(params[:futbol_id])
    @review = @futbol.reviews.new(review_params)
    if( @review.cantidad == 121)
        @review.visitantes = "1x2 - 1"
        @review.cantidad = @futbol.unoxdosuno
    elsif ( @review.cantidad == 120)
        @review.visitantes = "1x2 - x"
        @review.cantidad = @futbol.unoxdosequix
        elsif ( @review.cantidad == 122)
        @review.visitantes = "1x2 - 2"
        @review.cantidad = @futbol.unoxdosdos
    end
    
     @review.resultado =(@review.unoxdosuno) * (@review.cantidad)
     @review.local = SecureRandom.hex(5)
  if @review.save 
    redirect_to  futbol_review_url(@futbol,@review),
                  notice: "Thanks for your review!"
  else
    render :new
  end
  
 

end

private
  def review_params
    params.require(:review).permit(:cantidad, :unoxdosuno, :unoxdosdos, :resultado, :visitantes)
  end


end
