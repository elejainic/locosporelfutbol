class PartidosController < ApplicationController
  def new
     @futbol = Futbol.new
  end

  def create
    @futbol = Futbol.new(params[:futbol])
 
    @futbol.save
      redirect_to root
    
  
  end

  def show
    @futbol = Futbol.find(params[:id])
  end
  
  private
  def futbol_params
    params.require(:futbol).permit(:unoxcero, :dosxcero, :dosxuno, :tresxcero, :tresxuno, :tresxdos, :cuatroxcero, :cuatroxuno, :cuatroxdos, :cuatroxtres, :cincoxcero, :ceroxcero, :unoxuno, :dosxdos, :tresxtres, :cuatroxcuatro, :ceroxuno, :ceroxdos, :unoxdos, :ceroxtres, :unoxtres, :dosxtres, :ceroxcuatro, :unoxcuatro, :dosxcuatro, :tresxcuatro, :quiniela1, :quinielaempate, :quinielados)
  end
end

