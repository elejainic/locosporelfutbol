class Game < ActiveRecord::Base
    has_many :bets
    default_scope -> { order(created_at: :desc) }
    validates :local, :visitante, :unoxcero, :dosxcero, :dosxuno, :tresxcero, :tresxuno, :tresxdos, :cuatroxcero, :cuatroxuno, :cuatroxdos, :cuatroxtres, :cincoxcero, :ceroxcero, :unoxuno, :dosxdos, :tresxtres, :cuatroxcuatro, :ceroxuno, :ceroxdos, :unoxdos, :ceroxtres, :unoxtres, :dosxtres, :ceroxcuatro, :unoxcuatro, :dosxcuatro, :tresxcuatro, :presence => {:message => 'Name cannot be blank, Task not saved'}
end
