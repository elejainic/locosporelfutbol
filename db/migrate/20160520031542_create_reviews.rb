class CreateReviews < ActiveRecord::Migration
  def change
    create_table :reviews do |t|
      t.string :local
      t.string :visitantes
      t.decimal :unoxdosuno
      t.decimal :unoxdosequis
      t.decimal :unoxdosdos
      t.references :futbol, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
