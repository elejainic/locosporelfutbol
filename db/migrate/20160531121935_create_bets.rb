class CreateBets < ActiveRecord::Migration
  def change
    create_table :bets do |t|
      t.string :random
      t.decimal :cotizacion
      t.decimal :resultado
      t.decimal :intro
      t.references :game, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
