class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :local
      t.string :visitante
      t.decimal :unoxcero
      t.decimal :dosxcero
      t.decimal :dosxuno
      t.decimal :tresxcero
      t.decimal :tresxuno
      t.decimal :tresxdos
      t.decimal :cuatroxcero
      t.decimal :cuatroxuno
      t.decimal :cuatroxdos
      t.decimal :cuatroxtres
      t.decimal :cincoxcero
      t.decimal :ceroxcero
      t.decimal :unoxuno
      t.decimal :dosxdos
      t.decimal :tresxtres
      t.decimal :cuatroxcuatro
      t.decimal :ceroxuno
      t.decimal :ceroxdos
      t.decimal :unoxdos
      t.decimal :ceroxtres
      t.decimal :unoxtres
      t.decimal :dosxtres
      t.decimal :ceroxcuatro
      t.decimal :unoxcuatro
      t.decimal :dosxcuatro
      t.decimal :tresxcuatro
      t.decimal :otro
      t.decimal :cantidad

      t.timestamps null: false
    end
  end
end
