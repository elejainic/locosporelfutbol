class CreateFutbols < ActiveRecord::Migration
  def change
    create_table :futbols do |t|
      t.string :local
      t.string :visitante

      t.timestamps null: false
    end
  end
end
