class AddDetailsToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :cantidad, :decimal
    add_column :reviews, :resultado, :decimal
  end
end
