class CreateApuesta < ActiveRecord::Migration
  def change
    create_table :apuesta do |t|
      t.decimal :cantidad
      t.string :local
      t.string :visitante
      t.string :marcador
      t.decimal :cotizacion
      t.decimal :resultado
      t.references :futbol, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
