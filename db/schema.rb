# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160604010914) do

  create_table "apuesta", force: :cascade do |t|
    t.decimal  "cantidad"
    t.string   "local"
    t.string   "visitante"
    t.string   "marcador"
    t.decimal  "cotizacion"
    t.decimal  "resultado"
    t.integer  "futbol_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "apuesta", ["futbol_id"], name: "index_apuesta_on_futbol_id"

  create_table "bets", force: :cascade do |t|
    t.string   "random"
    t.decimal  "cotizacion"
    t.decimal  "resultado"
    t.decimal  "intro"
    t.integer  "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "valor"
  end

  add_index "bets", ["game_id"], name: "index_bets_on_game_id"

  create_table "futbols", force: :cascade do |t|
    t.string   "local"
    t.string   "visitante"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.decimal  "unoxdosuno"
    t.decimal  "unoxdosequix"
    t.decimal  "unoxdosdos"
    t.string   "column_fecha"
  end

  create_table "games", force: :cascade do |t|
    t.string   "local"
    t.string   "visitante"
    t.decimal  "unoxcero"
    t.decimal  "dosxcero"
    t.decimal  "dosxuno"
    t.decimal  "tresxcero"
    t.decimal  "tresxuno"
    t.decimal  "tresxdos"
    t.decimal  "cuatroxcero"
    t.decimal  "cuatroxuno"
    t.decimal  "cuatroxdos"
    t.decimal  "cuatroxtres"
    t.decimal  "cincoxcero"
    t.decimal  "ceroxcero"
    t.decimal  "unoxuno"
    t.decimal  "dosxdos"
    t.decimal  "tresxtres"
    t.decimal  "cuatroxcuatro"
    t.decimal  "ceroxuno"
    t.decimal  "ceroxdos"
    t.decimal  "unoxdos"
    t.decimal  "ceroxtres"
    t.decimal  "unoxtres"
    t.decimal  "dosxtres"
    t.decimal  "ceroxcuatro"
    t.decimal  "unoxcuatro"
    t.decimal  "dosxcuatro"
    t.decimal  "tresxcuatro"
    t.decimal  "otro"
    t.decimal  "cantidad"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "fecha"
  end

  create_table "reviews", force: :cascade do |t|
    t.string   "local"
    t.string   "visitantes"
    t.decimal  "unoxdosuno"
    t.decimal  "unoxdosequis"
    t.decimal  "unoxdosdos"
    t.integer  "futbol_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.decimal  "cantidad"
    t.decimal  "resultado"
    t.integer  "game_id"
  end

  add_index "reviews", ["futbol_id"], name: "index_reviews_on_futbol_id"
  add_index "reviews", ["game_id"], name: "index_reviews_on_game_id"

end
